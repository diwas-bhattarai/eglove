#include "Nrf24l01p.h"



Nrf24l01p::Nrf24l01p(PinName mosi, 
                     PinName miso, 
                     PinName sck, 
                     PinName ncsn,
                     PinName nce) : spi(mosi, miso, sck), csn(ncsn), ce(nce){
    Nucleo_Init();
    SPI_Init();

}

void Nrf24l01p::setPayLoadLength(uint8_t len){
	payloadLen = len;
    nRF_Init();
}

uint8_t Nrf24l01p::getPayLoadLength(void){
	return payloadLen;
}

void Nrf24l01p::nRF_Node_Select(uint8_t *addrData, uint8_t addrLen, uint8_t node)
{
	//Set the TX & RX address as 0x11 0x12 0x13 0x14 0x15
	nRF_Set_Addr_TX(addrData, addrLen, node);
	nRF_Set_Addr_RX(addrData, addrLen, node);
}

void  Nrf24l01p::nRF_Set_Addr_TX(uint8_t *addrData, uint8_t addrLen, uint8_t node)
{
    uint8_t i;
    wait_us(10);
    csn = 0;            //CSN low
    wait_us(10);
    //Setup p0 pipe address for receiving
    spi.write(W_REGISTER + TX_ADDR);
    for(i = 0; i < addrLen; i++)
    {
        wait_us(10);
        spi.write(addrData[i]);
    }
    wait_us(10);
    csn = 1;            //CSN high
}

void  Nrf24l01p::nRF_Set_Addr_RX(uint8_t *addrData, uint8_t addrLen, uint8_t node)
{
    uint8_t i;

    wait_us(10);
    csn = 0;            //CSN low
    wait_us(10);
    //Setup p0 pipe address for receiving
    spi.write(W_REGISTER + RX_ADDR_P0);
    for(i = 0; i < addrLen; i++)
    {
        wait_us(10);
        spi.write(addrData[i]);
    }
    wait_us(10);
    csn = 1;            //CSN high
}


/************************************************************************************
** Nucleo_Init function:
** - Start-up delay
** - Initializes the I/O peripherals
*************************************************************************************/
void Nrf24l01p::Nucleo_Init(void)
{
    wait(0.75);         //750ms mandatory delay for BNO055 POR
}

/************************************************************************************
** SPI_Init function:
** - 1MHz
** - Mode 0: ClockPhase = 0, ClockPolarity = 0
*************************************************************************************/
void Nrf24l01p::SPI_Init(void)
{
    spi.format(8,0);    //8 bit data, Mode 0
    spi.frequency(1000000);

    csn = 1;            //CSN high
    ce = 0;             //CE low
}

void Nrf24l01p::payloadsizeChange(uint8_t len)
{
	SPI_Write_Byte(RX_PW_P0, len);
	payloadLen = len;

}

void Nrf24l01p::nRF_Init(void)
{
    //Enable auto-acknowledgment for data pipe 0
    SPI_Write_Byte(EN_AA, 0x01);

    //Enable data pipe 0
    SPI_Write_Byte(EN_RXADDR, 0x01);

    //Set address width to 5 bytes
    SPI_Write_Byte(SETUP_AW, 0x03);

    //Set channel frequency to 2.505GHz
    SPI_Write_Byte(RF_CH, 0x69);

    //Set data rate to 250kbps and 0dB gain
    SPI_Write_Byte(RF_SETUP, 0x26);

	SPI_Write_Byte(FEATURE, 0x01);
	SPI_Write_Byte(FEATURE,SPI_Read_Byte(FEATURE) | (1 << EN_DPL) );
	SPI_Write_Byte(DYNPD,SPI_Read_Byte(DYNPD) |  (1 << DPL_P0) );

    //Set the payload width
	payloadsizeChange(getPayLoadLength());

    //Set the retransmission delay to 4000us with 15 retries
    SPI_Write_Byte(SETUP_RETR, 0xFF);

    //Boot the nrf as TX and mask the maximum retransmission interrupt(disable)
    //Enable CRC and set the length to 2-bytes
    nRF_TX_Mode();

    wait_ms(10);        //10ms delay after power-up
}

void Nrf24l01p::SPI_Write_Byte(unsigned char reg, unsigned char data)
{
    wait_us(10);
    csn = 0;            //CSN low
    wait_us(10);
    spi.write(W_REGISTER + reg);
    wait_us(10);
    spi.write(data);
    wait_us(10);
    csn = 1;            //CSN high
}

void Nrf24l01p::nRF_TX_Mode(void)
{
    ce = 0;                             //CE low - Standby-I
    //Power-up and set as TX
    SPI_Write_Byte(CONFIG, SPI_Read_Byte(CONFIG) & ~(1 << PRIM_RX));
    SPI_Write_Byte(CONFIG, SPI_Read_Byte(CONFIG) | (1 << PWR_UP));
    nRF_Flush_TX();                     //Flush TX FIFO
    SPI_Write_Byte(STATUS, (1 << RX_DR) | (1 << TX_DS) | (1 << MAX_RT)); //Reset status
    //Mask TX_DR and MAX_RT interrupts
    SPI_Write_Byte(CONFIG, SPI_Read_Byte(CONFIG) | (1 << MASK_TX_DS) | (1 << MASK_MAX_RT));
    wait_us(150);
}

unsigned char Nrf24l01p::SPI_Read_Byte(unsigned char reg)
{
    wait_us(10);
    csn = 0;            //CSN low
    wait_us(10);
    spi.write(R_REGISTER + reg);
    wait_us(10);
    reg = spi.write(NOP);
    wait_us(10);
    csn = 1;            //CSN high
    return reg;
}

void Nrf24l01p::nRF_Flush_TX(void)
{
    wait_us(10);
    csn = 0;            //CSN low
    wait_us(10);
    spi.write(FLUSH_TX);
    wait_us(10);
    csn = 1;            //CSN high
    wait_us(10);
}

void Nrf24l01p::nRF_IRQ_vect(Serial &pc, uint8_t *BS_payload_RX)
{
    ce = 0;                     //Stop listening
    //Pull down chip select
    csn = 0;                    //CSN low
    wait_us(10);
    //Send command to read RX payload
    spi.write(R_RX_PAYLOAD);
    wait_us(10);
    //Read payload
    nRF_get_Payload(pc, BS_payload_RX);
    wait_us(10);
    //Pull up chip select
    csn = 1;                    //CSN high
    wait_us(10);
    //Reset status register
    SPI_Write_Byte(STATUS, (1 << RX_DR));
}

void Nrf24l01p::nRF_get_Payload(Serial &pc, uint8_t *BS_payload_RX)
{
	uint8_t i;
    //pc.putc(START_FLAG);
    uint8_t len = getPayLoadLength();
    for(i = 0; i < len; i++)
    {
    	volatile uint8_t data_in = spi.write(0x00);
        //if (data_in == START_FLAG || data_in == END_FLAG || data_in == ESCAPE_FLAG) {
        //    pc.putc(ESCAPE_FLAG);
        //}
        pc.putc(data_in);
        BS_payload_RX[i] = data_in;
    }
    //pc.putc(END_FLAG);
}

void Nrf24l01p::nRF_Reset(void)
{
    wait_us(10);
    //Reset IRQ-flags in status register
    SPI_Write_Byte(STATUS, 0x70);
    wait_us(10);
}

void Nrf24l01p::nRF_Flush_RX(void)
{
    wait_us(10);
    csn = 0;            //CSN low
    wait_us(10);
    spi.write(FLUSH_RX);
    wait_us(10);
    csn = 1;            //CSN high
    wait_us(10);
}

uint8_t Nrf24l01p::nRF_is_Sending(void)
{
    uint8_t status;

    //Read the current status
    status = nRF_get_Status();


    //If sending successful (TX_DS) or max retries exceeded (MAX_RT)
    if((status & ((1 << TX_DS)  | (1 << MAX_RT))))
    {
        return 0;       //False
    }
    return 1;           //True
}

uint8_t Nrf24l01p::nRF_get_Status(void)
{
    uint8_t rv;
    csn = 0;            //CSN low
    rv = spi.write(NOP);
    csn = 1;            //CSN high
    return rv;
}

void Nrf24l01p::nRF_TX_Data(uint8_t *data, uint8_t len)
{
    nRF_Flush_TX();
    csn = 0;            //CSN low
    wait_us(10);
    //Transmit payload with ACK enabled
    spi.write(W_TX_PAYLOAD);
    wait_us(10);
    nRF_send_Payload(data, len);
    wait_us(10);
    csn = 1;            //CSN high
    wait_us(10);        //Need at least 10us before sending
    ce = 1;             //CE high
    wait_us(10);        //Hold CE high for at least 10us and not longer than 4ms
    ce = 0;             //CE low
    while(nRF_is_Sending());
}

void Nrf24l01p::nRF_send_Payload(uint8_t* data, uint8_t len)
{
    uint8_t i;
    for(i = 0; i < len; i++)
    {
    	spi.write(data[i]);
    }
}


void Nrf24l01p::nRF_RX_Mode(void)
{
    ce = 0;                             //CE low - Standby-I
    //Power-up as set as RX
    SPI_Write_Byte(CONFIG, SPI_Read_Byte(CONFIG) | (1 << PWR_UP) | (1 << PRIM_RX));
    nRF_Flush_RX();                     //Flush RX FIFO
    SPI_Write_Byte(STATUS, (1 << RX_DR) | (1 << TX_DS) | (1 << MAX_RT)); //Reset status
    //Mask TX_DR and MAX_RT interrupts
    SPI_Write_Byte(CONFIG, SPI_Read_Byte(CONFIG) | (1 << MASK_TX_DS) | (1 << MASK_MAX_RT));
    ce = 1;                             //CE high
    wait_us(150);
}

Nrf24l01p::~Nrf24l01p() {}
