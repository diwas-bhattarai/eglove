#ifndef __NRF24L01P_H__
#define __NRF24L01P_H__


#include "mbed.h"
#include "nrf.h"

class Nrf24l01p  {
public:
    Nrf24l01p(PinName mosi, PinName miso, PinName sck, PinName ncsn, PinName nce);
    void setPayLoadLength(uint8_t len);
    uint8_t getPayLoadLength(void);
    void nRF_Node_Select(uint8_t *addrData, uint8_t addrLen, uint8_t node);
    void nRF_IRQ_vect(Serial &pc, uint8_t *BS_payload_RX);
    void nRF_TX_Mode(void);
    void nRF_TX_Data(uint8_t *data, uint8_t len);
    void nRF_RX_Mode(void);
    void payloadsizeChange(uint8_t len);
    void nRF_Reset(void);
    void nRF_Flush_RX(void);
    void nRF_Flush_TX(void);
    ~Nrf24l01p();

private:
    SPI         spi;
    DigitalOut  csn;
    DigitalOut  ce;
    volatile uint8_t payloadLen;
    void Nucleo_Init(void);
    void SPI_Init(void);
    void nRF_Init(void);
    void SPI_Write_Byte(unsigned char reg, unsigned char data);
    unsigned char SPI_Read_Byte(unsigned char reg);

    void nRF_get_Payload(Serial &pc, uint8_t *BS_payload_RX);

    void nRF_Set_Addr_RX(uint8_t *addrData, uint8_t addrLen, uint8_t node);
    void nRF_Set_Addr_TX(uint8_t *addrData, uint8_t addrLen, uint8_t node);
    void nRF_send_Payload(uint8_t* data, uint8_t len);
    uint8_t nRF_is_Sending(void);
    uint8_t nRF_get_Status(void);
};

#endif /* __NRF24L01P_H__ */
