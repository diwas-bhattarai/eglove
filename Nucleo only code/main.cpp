#include "Nrf24l01p.h"


#define PAYLOAD_LEN 		20
#define PAYLOAD_QUAR_LEN 	10

#define NRF_TOTAL_NODES 	6
#define NRF_ADDR_LEN 		5

/* Change it new Nodes or Sub-nodes are made. */
#define TOTAL_NODES_AND_SUBNODES 2

uint8_t BS_payload_TX[PAYLOAD_LEN] = {0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
//uint8_t BS_payload_TX[PAYLOAD_LEN] = {0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA};
uint8_t BS_payload_RX[PAYLOAD_LEN];



/* add more address for nRF_Nodes and NRF_address  if more than 6 device available.
 * make sure change value for TOTAL_NODES_AND_SUBNODES, to connect to the max number of devices.
*/

uint8_t nRF_Nodes[NRF_TOTAL_NODES] = {0x01,0x02,0x03,0x04,0x05,0x06};   // Address of the Main Nodes connected to the Base station
uint8_t NRF_address[NRF_TOTAL_NODES][NRF_ADDR_LEN] = {   				// Address of Sub Nodes connected to the Main Nodes
    												 {0x11, 0x12, 0x13, 0x14, 0x15},
    												 {0x21, 0x22, 0x23, 0x24, 0x25},
    												 {0x31, 0x32, 0x33, 0x34, 0x35},
    												 {0x41, 0x42, 0x43, 0x44, 0x45},
    												 {0x51, 0x52, 0x53, 0x54, 0x55},
    												 {0x61, 0x62, 0x63, 0x64, 0x65}
    												 };

Nrf24l01p nrf(D11, D12, D13, D4, D5); 	// Create an object for the NRF24L01Plus class
Serial pc(SERIAL_TX, SERIAL_RX);		// Serial connection
Ticker flipper; 						// Ticker for mode transmission to other node if no packet received
InterruptIn nrf_irq(D6);				// Pin in Nrf24l01p for Interrupt
volatile uint8_t mode; 					// 0 - TX; 1 - RX
volatile uint8_t nRF_Node; 				// Node number
volatile uint8_t serialrun = 1;
volatile uint8_t serial_rx_Payload_cnt = 0;

Timer t;
volatile int timeTaken = 0;

void changereturndatatype(uint8_t count){
    if(count <= 10) {
    	nrf.payloadsizeChange(PAYLOAD_QUAR_LEN);
    	BS_payload_TX[10] = 0x00;
    	BS_payload_TX[11] = 0x00;
    	BS_payload_TX[12] = 0x00;
    	BS_payload_TX[13] = 0x00;
    	BS_payload_TX[14] = 0x00;
    	BS_payload_TX[15] = 0x00;
    	BS_payload_TX[16] = 0x00;
    	BS_payload_TX[17] = 0x00;
    	BS_payload_TX[18] = 0x00;
    	BS_payload_TX[19] = 0x00;
    }else if(count > 10){
    	nrf.payloadsizeChange(PAYLOAD_LEN);
    	BS_payload_TX[10] = 0xAA;
    	BS_payload_TX[11] = 0xAA;
    	BS_payload_TX[12] = 0xAA;
    	BS_payload_TX[13] = 0xAA;
    	BS_payload_TX[14] = 0xAA;
    	BS_payload_TX[15] = 0xAA;
    	BS_payload_TX[16] = 0xAA;
    	BS_payload_TX[17] = 0xAA;
    	BS_payload_TX[18] = 0xAA;
    	BS_payload_TX[19] = 0xAA;
    }
}

void onSerialInput(){
	char buffer[20];
		if(pc.readable()){
			pc.gets(buffer,19); // @suppress("Method cannot be resolved")
		}

	if (serialrun == 1){
		for(uint8_t i=0; i<PAYLOAD_LEN; i++)
		{
			if (buffer[i] == 0xAA)
			{
				serial_rx_Payload_cnt++;
			}
		}
		changereturndatatype(serial_rx_Payload_cnt);
		serialrun = 0;
		serial_rx_Payload_cnt = 0;
	}else{
		/* Interrupt is interrupted two times so just taking the first interrup as valid interrupt
		 * and again the third as the valid interrupt */
		serialrun = 1;
	}
	/* waiting seems to be necessary
	 * if no waiting is done then no data transfer occurs so USB unplug and plug is required of the base station
	 * */
	wait(1);
}

void modeTransmit(void)
{
	t.stop();
	flipper.detach();
    mode = 0;
    pc.attach(&onSerialInput);
    timeTaken = t.read_ms();
    t.reset();
}


void onDataReceived(void)
{
	flipper.detach();
	nrf_irq.disable_irq();
	/* really donot need BS_payload_RX at this point. Just for debug purpose. */
	nrf.nRF_IRQ_vect(pc, BS_payload_RX);
	//pc.printf("%x",BS_payload_RX[1]);
	t.stop();
	timeTaken = t.read_us();
	t.reset();
	mode = 0;
}

int main()
{
	pc.baud(115200);					// Set Serial baud-rate
	nrf.setPayLoadLength(PAYLOAD_QUAR_LEN);	// Set pay load length
    nrf_irq.fall(&onDataReceived);    	// Attach the address of the nRF IRQ Interrupt function to the falling edge
    mode = 0;  							// 0 - TX; 1 - RX
    nRF_Node = 0; 						// Initialize the node number
    nrf_irq.disable_irq();  			// Disable nRF IRQ initially
    pc.attach(&onSerialInput, Serial::RxIrq);
    while(1)
    {
    	serialrun = 1;
    	if((mode == 0))             	// Check if TX
    	        {
    				t.start();
    	        	nrf.nRF_Node_Select(NRF_address[nRF_Node],NRF_ADDR_LEN,nRF_Nodes[nRF_Node]); // select the node to talk
    	            nRF_Node++;
    	        	if (nRF_Node>=TOTAL_NODES_AND_SUBNODES){
    	        		nRF_Node = 0;
    	        		//onSerialInput('u');
    	            }

    	            nrf.nRF_TX_Mode(); 						// Configure as Transmitter
    	            nrf.nRF_TX_Data(BS_payload_TX, sizeof(BS_payload_TX));

    	            mode = 1;
    	            nrf.nRF_RX_Mode(); 						// Configure as Receiver
    	            nrf.nRF_Flush_RX();
    	            nrf.nRF_Reset();
    	            nrf_irq.enable_irq();   				// Enable nRF IRQ and start listening
    	            /* This time out should be calculated according to the time
    	             * required to receive the max byte array
    	            */
    	            flipper.attach(&modeTransmit, 0.032);   // Enable ticker

    	        }
    }
}

